/*
 * Copyright (c) 2020 Jannis Scheibe <jannis@tadris.de>
 *
 * This file is part of FitoTrack
 *
 * FitoTrack is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     FitoTrack is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

apply plugin: 'com.android.application'

allprojects {
    dependencies {
        repositories {
            jcenter()
            maven { url 'https://jitpack.io' }
        }
    }
}

android {
    compileSdkVersion 29
    buildToolsVersion "29.0.3"
    defaultConfig {
        applicationId "de.tadris.fitness"
        minSdkVersion 21
        targetSdkVersion 29
        versionCode 830
        versionName "8.3"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
        debug {
            applicationIdSuffix = ".debug"
        }
    }
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
        // Flag to enable support for the new language APIs
        coreLibraryDesugaringEnabled true
    }
    lintOptions {
        checkReleaseBuilds false
        // Or, if you prefer, you can continue to check for errors in release builds,
        // but continue the build even when errors are found:
//        abortOnError false
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])

    // Android
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.2'
    implementation 'androidx.appcompat:appcompat:1.2.0'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'androidx.documentfile:documentfile:1.0.1'

    // Maps
    def mapsforge_version = "0.14.0"
    implementation "org.mapsforge:mapsforge-core:$mapsforge_version"
    implementation "org.mapsforge:mapsforge-map:$mapsforge_version"
    implementation "org.mapsforge:mapsforge-map-reader:$mapsforge_version"
    implementation "org.mapsforge:mapsforge-themes:$mapsforge_version"
    implementation "org.mapsforge:mapsforge-map-android:$mapsforge_version"
    implementation 'com.caverock:androidsvg:1.4'

    // UI
    implementation 'net.sf.kxml:kxml2:2.3.0'
    implementation 'com.github.PhilJay:MPAndroidChart:v3.1.0'
    implementation 'com.github.clans:fab:1.6.4'

    // XML
    implementation 'stax:stax-api:1.0.1'
    implementation 'com.fasterxml.jackson.dataformat:jackson-dataformat-xml:2.9.8'

    // File Utils
    implementation 'commons-io:commons-io:2.6'

    // Upload to OSM
    implementation('de.westnordost:osmapi-traces:1.0')
    configurations {
        compile.exclude group: 'net.sf.kxml', module: 'kxml2' // already included in Android
    }

    // Parsing date strings in GPX-files
    implementation 'com.github.sisyphsu:dateparser:1.0.4'
    annotationProcessor 'org.projectlombok:lombok:1.18.8'

    // About Screen
    implementation 'com.github.medyo:android-about-page:1.3'

    // Bluetooth Low Energy (e.g. for heart rate support)
    implementation 'no.nordicsemi.android:ble:2.2.4'
    implementation 'no.nordicsemi.android:ble-common:2.2.4'

    // Android Room Database
    def room_version = "2.2.5"
    annotationProcessor "androidx.room:room-compiler:$room_version"
    implementation "androidx.room:room-runtime:$room_version"

    // support for the new language APIs
    coreLibraryDesugaring 'com.android.tools:desugar_jdk_libs:1.0.10'

    testImplementation 'junit:junit:4.13'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'
}
